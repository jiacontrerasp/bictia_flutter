import 'package:prueba_bictia/models/chat.dart';

List<Chat> dummyData = [
  new Chat(
    avatar: 'https://secureservercdn.net/198.71.233.161/3e3.747.myftpupload.com/wp-content/uploads/2018/03/bictia.jpg',
    title: 'Grupo FullStack Bictia',
    message: 'Hola Equipo, ¿cómo están?',
    time: '15:14'    
  ),
  new Chat(
    avatar: 'https://i.pinimg.com/736x/e3/3c/2f/e33c2fa94c03efa06678116f80d62d0d.jpg',
    title: 'Amigos de gokú',
    message: '¿Tienen 5 minutos para halar del señor Gokú?',
    time: '15:13'    
  ),

];