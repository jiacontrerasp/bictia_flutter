import 'package:flutter/material.dart';
import 'package:prueba_bictia/pages/calls.dart';
import 'package:prueba_bictia/pages/chats.dart';
import 'package:prueba_bictia/pages/home.dart';
import 'package:prueba_bictia/pages/settings.dart';
import 'package:prueba_bictia/pages/status.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        //primarySwatch: Colors.blue,
        primaryColor: Colors.teal[500],
        primaryColorDark: Colors.teal[900],
        accentColor: Colors.lightGreen[500]
      ),
      home: MyHomePage(title: 'What\'s upppppp man'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  int _counter = 0;
  int _currentPage = 0;
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 3, initialIndex: 0);
  }

  List<Widget> _pages = [ChatPage(), StatusPage(), CallPage()];

  void _incrementCounter() {
    setState(() {
      _counter--;
    });
  }

  void _changePage(int page) {
    setState(() {
      _currentPage = page;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        bottom: TabBar(
          controller: _tabController,
          tabs: <Widget>[
            Tab(
              text: 'Chats',
            ),
            Tab(
              text: 'Status',
            ),
            Tab(
              text: 'Calls',
            )
          ],
        ),
      ),
      body: TabBarView(children: _pages, controller: _tabController,),
      /*
      body: _pages[_currentPage],
       bottomNavigationBar: BottomNavigationBar(
        onTap: _changePage,
        currentIndex: _currentPage,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.accessible), title: Text('Accesibilidad')),
          BottomNavigationBarItem(icon: Icon(Icons.account_balance),title: Text('Cuenta') )
        ]
        ), */
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.message, color: Colors.white)
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
