import 'package:flutter/material.dart';
import 'package:prueba_bictia/dummies/dummy.dart';

class ChatPage extends StatefulWidget {
  @override
  State<ChatPage> createState() {
    // TODO: implement createState
    return ChatState();
  }
}

class ChatState extends State<ChatPage> {
  void hacerAlgo() {
    setState(() {
      print('Hice click');
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new ListView.builder(
        itemCount: dummyData.length,
        itemBuilder: (context, index) => new Column(
              children: <Widget>[
                ListTile(
                    onTap: hacerAlgo,
                    leading: CircleAvatar(
                      backgroundImage:
                          new NetworkImage(dummyData[index].avatar),
                    ),
                    title: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          dummyData[index].title,
                          style: new TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Text(
                          dummyData[index].time,
                          style: new TextStyle(
                              fontSize: 12, color: Colors.grey[400]),
                        ),
                      ],
                    ),
                    subtitle: Text(dummyData[index].message)),
                Divider(
                  height: 10,
                ),
              ],
            ));
  }
}
